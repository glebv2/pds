from setuptools import setup
setup(
    name='pds',
    version='1.6',
    description='Pds server',
    url='https://git.glebmail.xyz/PythonPrograms/pcsd',
    author='gleb',
    packages=['pds'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
    scripts=['bin/pds']
)
